---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
<div id="personas-alertadoras" class="uk-section section-no-color animated fadeIn delay-4s fast">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
      <div class="uk-width-2-3@m">
      <h2>{% t home.who.title %}</h2>
      <p>{% t home.who.description_1 %}</p>
      <p>{% t home.who.description_2 %}</p>
      <p>{% t home.who.description_3 %}</p>
      <p>{% t home.who.description_4 %}</p>
      </div>
      <div class="uk-visible@m uk-width-1-3@m uk-text-center">
        <img data-src="/assets/images/icon-latamleaks.svg" uk-img style="width:60%;">
      </div>
      <div class="uk-width-1-1">
        <iframe width="1920" height="1080" src="https://www.youtube.com/embed/q26s7u-VfOg" frameborder="0" allowfullscreen uk-responsive uk-video="automute: true"></iframe>
      </div>
    </div>
  </div>
</div>

<div id="sobre-latamleaks" class="uk-section section-color-1st-6 animated fadeIn">
  <div class="uk-container uk-width-3-4@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>{% t home.about.title %}</h2>
        <p style="margin-bottom:30px">{% t home.about.description %}</p>
      </div>
    </div>
    <div class="uk-visible@m">
      <div class="uk-text-center uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <img data-src="/assets/images/logo-fibgar-black.svg" style="width:25%" uk-img>
        <img data-src="/assets/images/logo-fci-black.svg" style="width:20%" uk-img>
        <img data-src="/assets/images/logo-poder-black.png" style="width:16%" uk-img>
        <img data-src="/assets/images/logo-win-black.png" style="width:22%;margin-top:-10px" uk-img>
        <img data-src="/assets/images/logo-pplaaf-black.png" style="width:14%" uk-img>
      </div>
    </div>
    <div class="uk-flex uk-flex-center uk-flex-middle uk-text-center uk-hidden@m" style="margin-top:30px" uk-grid>
      <div class="uk-width-1-2">
        <img data-src="/assets/images/logo-fibgar-black.svg" uk-img>
      </div>
      <div class="uk-width-1-2">
        <img data-src="/assets/images/logo-fci-black.svg" uk-img>
      </div>
      <div class="uk-width-1-2">
        <img data-src="/assets/images/logo-poder-black.png" style="width:70%" uk-img>
      </div>
      <div class="uk-width-1-2">
        <img data-src="/assets/images/logo-win-black.png" uk-img>
      </div>      
      <div class="uk-width-1-3">
        <img data-src="/assets/images/logo-pplaaf-black.png" uk-img>
      </div>       
    </div>     
  </div>
</div>

<div id="plataformas" class="uk-section section-no-color  animated fadeIn">
  <div class="uk-container uk-width-2-3@m " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-2-3">
        <h2 class="uk-text-center">{% t home.latam.title %}</h2>
      </div>
    </div>
    <div class="uk-flex uk-flex-center" uk-grid>      
      <div class="uk-width-1-1">
        <div class="uk-inline uk-visible@m">
          <img data-src="/assets/images/map-latam.svg" uk-img>
          <div class="uk-position-absolute uk-transform-center" style="left: 80%; top: 20%; width:460px">
            <p class="font-little-title">{% t home.latam.mexico_country %}</p>
            <h4 style="margin:-15px 0 5px">{% t home.latam.mexico_title %}</h4>
            <p style="margin:0; font-size: 0.8rem" class="font-compressed">{% t home.latam.mexico_description %}</p>
            <a href="https://mexicoleaks.mx/" target="_blank" class="font-small font-mono font-color-1st-0">{% t home.latam.action %}</a>
          </div>
          <div class="uk-position-absolute uk-transform-center" style="left: 9%; top: 39%; width:310px">
            <p class="font-little-title">{% t home.latam.guatemala_country %}</p>
            <h4 style="margin:-15px 0 5px">{% t home.latam.guatemala_title %}</h4>
            <p style="margin:0 ; font-size: 0.8rem" class="font-compressed">{% t home.latam.guatemala_description %}</p>
            <a href="https://guatemalaleaks.org/" target="_blank" class="font-small font-mono font-color-1st-0">{% t home.latam.action %}</a>
          </div>
          <div class="uk-position-absolute uk-transform-center" style="left: 24%; top: 68%; width:310px">
            <p class="font-little-title">{% t home.latam.nicaragua_country %}</p>
            <h4 style="margin:-15px 0 5px">{% t home.latam.nicaragua_title %}</h4>
            <p style="margin:0 ; font-size: 0.8rem" class="font-compressed">{% t home.latam.nicaragua_description %}</p>
            <a href="https://subterraneoni.org/" target="_blank" class="font-small font-mono font-color-1st-0">{% t home.latam.action %}</a>
          </div>
          <div class="uk-position-absolute uk-transform-center" style="left: 102%; top: 52%; width:310px">
            <p class="font-little-title">{% t home.latam.peru_country %}</p>
            <h4 style="margin:-15px 0 5px">{% t home.latam.peru_title %}</h4>
            <p style="margin:0 ; font-size: 0.8rem" class="font-compressed">{% t home.latam.peru_description %}</p>
            <!-- <a href="http://leaks.pe/" target="_blank" class="font-small font-mono font-color-1st-0">{% t home.latam.action %}</a> -->
          </div>
          <div class="uk-position-absolute uk-transform-center" style="left: 89%; top: 83%; width:352px">
            <p class="font-little-title">{% t home.latam.chile_country %}</p>
            <h4 style="margin:-15px 0 5px">{% t home.latam.chile_title %}</h4>
            <p style="margin:0 ; font-size: 0.8rem" class="font-compressed">{% t home.latam.chile_description %}</p>
            <a href="https://chileleaks.org/index.html" target="_blank" class="font-small font-mono font-color-1st-0">{% t home.latam.action %}</a>
          </div>                                           
        </div>
        <div class="uk-hidden@m">
        <div>
          <p class="font-little-title">{% t home.latam.mexico_country %}</p>
          <h4 style="margin:-15px 0 5px">{% t home.latam.mexico_title %}</h4>
          <p style="margin:0; font-size: 0.8rem" class="font-compressed">{% t home.latam.mexico_description %}</p>
          <a href="https://mexicoleaks.mx/" target="_blank" class="font-small font-mono font-color-1st-0">{% t home.latam.action %}</a>
        </div><br>
        <div>
          <p class="font-little-title">{% t home.latam.guatemala_country %}</p>
          <h4 style="margin:-15px 0 5px">{% t home.latam.guatemala_title %}</h4>
          <p style="margin:0 ; font-size: 0.8rem" class="font-compressed">{% t home.latam.guatemala_description %}</p>
          <a href="" target="_blank" class="font-small font-mono font-color-1st-0">{% t home.latam.action %}</a>
        </div><br>
        <div>
          <p class="font-little-title">{% t home.latam.nicaragua_country %}</p>
          <h4 style="margin:-15px 0 5px">{% t home.latam.nicaragua_title %}</h4>
          <p style="margin:0 ; font-size: 0.8rem" class="font-compressed">{% t home.latam.nicaragua_description %}</p>
          <a href="https://subterraneoni.org/" target="_blank" class="font-small font-mono font-color-1st-0">{% t home.latam.action %}</a>
        </div><br>
        <div>
          <p class="font-little-title">{% t home.latam.peru_country %}</p>
          <h4 style="margin:-15px 0 5px">{% t home.latam.peru_title %}</h4>
          <p style="margin:0 ; font-size: 0.8rem" class="font-compressed">{% t home.latam.peru_description %}</p>
          <a href="http://leaks.pe/" target="_blank" class="font-small font-mono font-color-1st-0">{% t home.latam.action %}</a>
        </div><br>
        <div>
          <p class="font-little-title">{% t home.latam.chile_country %}</p>
          <h4 style="margin:-15px 0 5px">{% t home.latam.chile_title %}</h4>
          <p style="margin:0 ; font-size: 0.8rem" class="font-compressed">{% t home.latam.chile_description %}</p>
          <a href="https://chileleaks.org/index.html" target="_blank" class="font-small font-mono font-color-1st-0">{% t home.latam.action %}</a>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="leyes" class="uk-section section-color-1st-0 animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>{% t home.laws.title %}</h2>
        <p>{% t home.laws.description_1 %}</p>
        <p class="font-lead">{% t home.laws.description_2 %}</p>
      </div>
    </div>
  </div>
</div>

<div id="informe" class="uk-section section-no-color animated fadeIn">
  <div class="uk-container uk-width-3-4@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2 style="margin-bottom: 30px">{% t home.report.title %}</h2>
      </div>
    </div>
    <div class="uk-flex uk-flex-center uk-flex-middle">
      <div class="uk-child-width-1-2 uk-child-width-1-4@m uk-text-center uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div>
          <i class='uil uil-file-alt' style="font-size:50px"></i>
          <p class="font-little-title" style="margin-top:-10px">{% t home.report.document_1 %}</p>
          <a href="/assets/informe-juridico.pdf" target="_blank" class="uk-button button-color-1st uk-button-small">
            {% t home.report.action %}
          </a>
        </div>
        <div>
          <i class='uil uil-file-alt' style="font-size:50px"></i>
          <p class="font-little-title" style="margin-top:-10px">{% t home.report.document_2 %}</p>
          <a href="/assets/resumen-ejecutivo-argentina.pdf" target="_blank" class="uk-button button-color-1st uk-button-small">
            {% t home.report.action %}
          </a>
        </div>
        <div>
          <i class='uil uil-file-alt' style="font-size:50px"></i>
          <p class="font-little-title" style="margin-top:-10px">{% t home.report.document_3 %}</p>
          <a href="/assets/resumen-ejecutivo-colombia.pdf" target="_blank" class="uk-button button-color-1st uk-button-small">
            {% t home.report.action %}
          </a>
        </div>
        <div>
          <i class='uil uil-file-alt' style="font-size:50px"></i>
          <p class="font-little-title" style="margin-top:-10px">{% t home.report.document_4 %}</p>
          <a href="/assets/resumen-ejecutivo-mexico.pdf" target="_blank" class="uk-button button-color-1st uk-button-small">
            {% t home.report.action %}
          </a>
        </div>                                      
      </div>
    </div>
  </div>
</div>

<div id="contacto" class="uk-section section-gradient animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>{% t home.contact.title %}</h2>
        <p>{% t home.contact.description %}</p>
        <a href="mailto:empezamos@latamleaks.lat"><button class="uk-button button-white-color-2nd">{% t home.contact.mail %}
</button></a>
      </div>
    </div>
  </div>
</div>
