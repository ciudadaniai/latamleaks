# Template de páginas en Jekyll para Ciudadanía Inteligente

Este template puede ser visto en https://ciudadaniai.gitlab.io/template-jekyll/

## Cómo comenzar un repositorio a partir de este template

- Descargar este repositorio: para ello en la página principal seleccionar Download > Download source code. 
- Descomprimir los archivos y darle el nombre a la carpeta del proyecto.
- Crear un nuevo repositorio en el gitlab de la organización [https://gitlab.com/ciudadaniai] para subir estos archivos.
- Conectar la carpeta local del proyecto con el repositorio git. Para ello *pushear* la carpeta existente:

```
cd <nombre-carpeta> //Entrar, utilizando la terminal, en el carpeta donde se encuentra el proyecto localmente
git init
git remote add origin git@gitlab.com:ciudadaniai/<nombre-repositorio>.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
- Eliminar la sección recién leída del README para no confundir en el nuevo proyecto.


## Instalación

- Install jekyll
```
gem install bundler jekyll
```
- Clone repository
```
git@gitlab.com:ciudadaniai/template-jekyll.git
```
- ` cd template-jekyll` to enter the site
- ` bundle install` to install the gems
- Ready!
```
bundle exec jekyll serve --watch --baseurl=    
```
